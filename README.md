# RAPiD

Collection of scripts used for **R**apid and **A**ccurate Structure-Based Therapeutic **P**eptide **D**esign using GPU Accelerated Thermodynamic Integration. 

![workflow](./docs/fig1.png)
*NOTE: Some knowledge of shell scripting, Linux systems and, Amber package will be of use. 



# Installation

To use these scripts, simply copy them into the desired directory and add that directory to your PATH. It is recommended to use these scripts on a Linux system.

## Dependencies

  - [Amber](http://ambermd.org/) 16 or later.
  - [Cluster from MMTSB](http://blue11.bch.msu.edu/mmtsb/Cluster.pl)
  - Python 3.X is recommended, although the code should work fine on 2.7.  [Anaconda](https://anaconda.org/anaconda/python) 

## Python Requirements

  - [numpy](http://www.numpy.org/)
  - [pandas](https://pandas.pydata.org/)
  - [matplotlib](https://matplotlib.org/)
  - [seaborn](https://seaborn.pydata.org/)
  - [scipy](https://www.scipy.org/)
  - [pytaj](https://amber-md.github.io/pytraj/latest/index.html)
  - [pyRosetta](http://www.pyrosetta.org/dow)

## Optional

  - [pmemdGTI](https://pubs.acs.org/doi/abs/10.1021/acs.jctc.7b00102) An Efficient Implementation of GPU-Accelerated Thermodynamic Integration



# Quick Sampling of Mutations Step.

The main goal of this step is to narrow down the sequence space to explore with TI a set of potentially enriched favorable mutations. We are aware that Rosetta has the potential to provide much better results by applying complex protocols. However,  here we preferred to prioritize speed, losing some good candidates in the way. Rosetta folder contains scripts used to explore single point mutations of canonical and non-canonical amino acids. From the whole catalog of NCAA within Rosetta only a subsample of NCAA where finally explored. We preferred to focus on those already parametrized on Amber at the time of this work, check the list at  ```./docs/NCAA_Table.xlsx```.


## Usage

This folder contains Rosetta scripts used to explore single point mutations of canonical and non-canonical amino acids. From the whole catalog of NCAA within Rosetta only a subsample of NCAA where finally explored. We preferred to focus on those already parametrized on Amber at the time of this work, check the list at  ```./docs/NCAA_Table.xlsx```.



For canonical single point mutation, the scrip ```/rosetta/caa_mutation_scan.py``` can be used. this script performs a basic and quick single point mutation and quantifies the free energy differences (in Rosetta energy units). This script was designed to be applied over the centroids of EME1-MUS81, but could be applied to other systems with very basic modifications.  It creates the resfile, mutates the complex, repacks and scores the difference. 

```bash

python caa_mutation_scan.py eme1_mus81_centroid_300.pdb 

```

For the non-canonical amino acids, we used a little bit more elaborated script, with a longer relaxation and repacking.  ```rosetta/ncaa_mutation_scan.py``` . It needs a pdb and a resfile. You can create a resfile using the script just_resfile.py If any other parameter is passed, the script will calculate the delta score of the wildtype. No mutation will be performed, only the relaxation and repacking steps. 

```bash
python scan_binding_energies_mut_resfile.py --help

Options:
  -h, --help           show this help message and exit
  --pdb=PDB_FILENAME    PDB file.This file is mandatory.
  --mut_pos=MUT_POST    Mutation position, use pdb order and numbers. If is not provided the mutation is not performed
  --mut_aa=MUT_AA       Target amino acid in the 3 letter code. if it is not provided the mutation is not performed
  --replica=REP         replica number. If it is not provided it will be labeled as 0
  --chain_lig=CHAIN_LIG Chain of the ligand or mutation molecule. This is a mandatory field.
  --resfile=RESFILE     resfile, that belongs to the pdb. This file is mandatory. Use just_resfile.py to create one
  --cycles=CYCLES       Number of relaxation cycles, by default 10.

```

A plain list of all non-canonical amino acids in rosetta, some of them will fail, bad definitions, etc. This list was finally cut it off to run only NCAA already parametrized in amber see ```/docs/NCAA_table.xlsx```

# Thermodynamic Integrations Step.

Alchemical free energy calculations provide a means for the accurate determination of free energies from atomistic simulations and are increasingly used as a tool for computational studies of protein-ligand interactions. Thermodynamic integration (TI) is a method used to compare the difference in free energy between two given states (e.g., A and B) whose potential energies have different dependences on the spatial coordinates. Because the free energy of a system is not simply a function of the phase space coordinates of the system but is instead a function of the Boltzmann-weighted integral over phase space (i.e. partition function), the free energy difference between two states cannot be calculated directly. In thermodynamic integration, the free energy difference is calculated by defining a thermodynamic path between the states and integrating over ensemble-averaged enthalpy changes along the path. Such paths can either be real chemical processes or alchemical processes.  The "alchemy” term is used because it involves simulating a transformation of one chemical system to a different one along a nonphysical path to compute the free energy change associated with the transformation.

  Jorge, M., Garrido, N., Queimada, A., Economou, I., and Macedo, E. (2010) Effect of the Integration Method on the Accuracy and Computational Efficiency of Free Energy Calculations Using Thermodynamic Integration. J. Chem. Theo. Comp. 6, 1018–1027.

  Shyu, C., and Ytreberg, F. M. (2009) Reducing the bias and uncertainty of free energy estimates by using regression to fit thermodynamic integration data. J. Comp. Chem. 30, 2297–230



## Usage

These scripts have the aim to speed up the setup and analysis of  TI on amber. They mostly perform simple tasks, eg, renaming atom or residue names,  writing standard configuration files, and run Tleap and other tools to calculate a ΔΔG using TI for  a mutation of interest in a peptide-protein system.  The second group of scripts, Analysis,  have the intention of quick parsing and visualization of the most important elements of the TI. Scripts to setup,  and analyze TI using Amber 16 or later.

## **SETUP SCRIPTS**

  * *Binding*

There are 2 different protocols, single step and multiple steps. Multiple step protocols run independently for wildtype discharge, vdw+bonded, and mutation recharge.  Three-step is more resource consuming, however, it trend to provide more accurate dG for mutations where a charge is involved. Single topology using soft core potentials is used for both protocols. These scripts intend to provide a generic solution, under certain circumstances custom tuning must need it. 

```/binding/setup_ti_1step.py``` script will write all inputs for Amber run a one-step TI, and ```/binding/setup_ti_3step.py``` will do it for three-steps. The only requirements in both scripts are to provide a PDB file with the target structure, and a separate file with the ligand, and the mutation. 
Amber renumber files assigning 1 to the first amino acid in the PDB, please use this numbering to indicate the script the mutation. 
Although according to the literature 6 lambdas may provide good results, By default these scripts use 11 lambdas. We recommend running any number between 9 and 20.  

```bash

setup_ti_single.py --target targt.pdb --ligand ligre_ext.pdb --mutation PRO12ALA

setup_ti_multi.py --target targ_init.pdb --ligand lig_init.pdb --mutation ASP1THR

```

Options are common in both scripts. 

```bash
usage: setup_ti_single.py [-h] [--ligand LIGAND] [--chain CHAIN]
                          [--mutation MUTATION] [--target TARGET]
                          [--increment INCREMENT]
                          [--scmask-ignore SCMASK_IGNORE]
                          [--decouple-mask DECOUPLE_MASK] [--psteps PSTEPS]
                          [--ouput-folder OUTPUT_FOLDER]

optional arguments:
  -h, --help            show this help message and exit
  --ligand LIGAND
  --chain CHAIN
  --mutation MUTATION
  --target TARGET
  --increment INCREMENT
  --scmask-ignore SCMASK_IGNORE
  --decouple-mask DECOUPLE_MASK
  --psteps PSTEPS
  --ouput-folder OUTPUT_FOLDER


```
 * *Stability*

This protocol is based on this paper [Protein Thermostability Calculations Using Alchemical Free Energy Simulations](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2872215/). This scipts are currently   in early development and are not guaranteed to function properly. 
The unfolded state of a protein chain is difficult to model, as no single unfolded conformation exists. In previous work , unfolded states have been approximated with short peptides that turned out to produce reasonable results. Instead of using the particular sequence around the amino acid of interest, we chose GXG peptides, where X is the amino acid of interest, with capped termini as a reference state. 
The protein stability differences ΔΔG were calculated according to ΔGunfolded-ΔGfolded, as shown in the thermodynamic cycle in Fig. 2. Therefore, destabilizing mutations have a negative ΔΔG


```bash

setup_ti_single_stability.py --target trg.pdb --mutation PHE20LEU

```


```bash
usage: setup_ti_single_stability.py [-h] [--chain CHAIN] [--mutation MUTATION]
                                    [--target TARGET] [--increment INCREMENT]
                                    [--scmask-ignore SCMASK_IGNORE]
                                    [--decouple-mask] [--psteps PSTEPS]
                                    [--ouput-folder OUTPUT_FOLDER]

optional arguments:
  -h, --help            show this help message and exit
  --chain CHAIN
  --mutation MUTATION
  --target TARGET
  --increment INCREMENT
  --scmask-ignore SCMASK_IGNORE
  --decouple-mask
  --psteps PSTEPS
  --ouput-folder OUTPUT_FOLDER




```

## **ANALYSIS SCRIPTS**

Within the analysis folder there are scripts for general analysis of Molecular dynamics, parse the clustering output, and analyse the TI ouput.

```bash
cluster.pl  -kclust -radius 1 frame.pdb.* -log cluster.log -selmode heavy > cluster.output
```

After runing the cluster.pl tool from MMSTB, the centroids and cluster information can be parsed and extracted using the script ```/analysis/parse_kclust.py ```

Scripts to preprocess Amber TI output of one-step ```/analysis/analysis_onestep.py``` and three-steps ```/analysis/analysis_multistep.py```. Both scripts will return plots, lambdas averages, and final deltaGΔΔG. Recommend to skip the first 500 steps (1 ns if integration step is set to 2fs) to improve accuracy. It is strongly recommended to check the trands and desviation and error of the lambdas (analysis/plots). Each mutation may requere to skip a noise lambda.  adjusted for each particular trajectory. 

```bash

analysis_multistep.py --skip 500

analysis_onestep.py --skip 500
```

Options are common in both scripts. Add the flag --stbl if you want to get the ΔΔG from a stability TI.  


```bash
usage: analysis_onestep.py [-h] [--no-extrapolation]
                           [--ignore [IGNORE [IGNORE ...]]] [--skip SKIP]
                           [--limit LIMIT] [--no-plots] [--no-rms]

optional arguments:
  -h, --help            show this help message and exit
  --no-extrapolation    by default if lambda 0 or 1 is missing the value is
                        extrapolated. this argument disables this behaibour
  --ignore [IGNORE [IGNORE ...]]
                        Ignore a lambda or lambas, format -> step-lambda step-lambda2 e.g. complex-0.100 
  --skip SKIP           skip # of step to calculate the average on each
                        lambda, 500 or 1ns recommended value, default = 0
  --limit LIMIT         limit on the # of step to calculate the average dvdl
                        for each lambda, default until the end
  --no-plots            do not generate plots, only the ddg
  --no-rms              do not generate RMSD and RMSF data

```


# Authors

Michael Garton, Carles Corbi-Verge, and Philip M. Kim


# References

Rapid and Accurate Structure-Based Therapeutic Peptide Design using GPU Accelerated Thermodynamic Integration.  
Michael Garton, Carles Corbi-verge, Yuan Hu, Satra Nim, Nadya Tarasova, Brad Sherborne, Philip M. Kim 



# Acknowledgments

  Alexey Strokach


# Bibliography

A GPU-Accelerated Parameter Interpolation Thermodynamic Integration Free Energy Method.
Giese TJ1, York DM1. J Chem Theory Comput. 2018 Mar 13;14(3):1564-1582. doi: 10.1021/acs.jctc.7b01175. Epub 2018 Feb 7.

Evaluating Thermodynamic Integration Performance of the New Amber Molecular Dynamics Package and Assess Potential Halogen Bonds of Enoyl-ACP Reductase (FabI). 
Benzimidazole Inhibitors. Pin-Chih Su1 and Michael E. Johnson1. J Comput Chem. 2016 Apr 5; 37(9): 836–847.

Rapid, Accurate, Precise, and Reliable Relative Free Energy Prediction Using Ensemble Based Thermodynamic Integration.
Agastya P. Bhati , Shunzhou Wan , David W. Wright , and Peter V. Coveney* J. Chem. Theory Comput., 2017, 13 (1), pp 210–222

Guidelines for the analysis of free energy calculations. 
Pavel V. Klimovich, Michael R. Shirts, and David L. Mobley. J Comput Aided Mol Des. 2015 May; 29(5): 397–411.

Soft-Core Potentials in Thermodynamic Integration. Comparing One- and Two-Step Transformations
Thomas Steinbrecher,‡ In Suk Joung,★ and David A. Case★. J Comput Chem. 2011 Nov 30; 32(15): 3253–3263.

Evaluating marginal likelihood with thermodynamic integration method and comparison with several other numerical methods
Peigui Liu  Ahmed S. Elshall  Ming Ye  Peter Beerli  Xiankui Zeng  Dan Lu  Yuezan Tao

Towards Accurate Free Energy Calculations in Ligand Protein-Binding Studies. 
Thomas Steinbrecher and Andreas Labahn*. Current Medicinal Chemistry, 2010, 17, 767-785 767
