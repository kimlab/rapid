from __future__ import print_function
import sys
import os
import logging
import optparse  
import time

#os.environ["PYROSETTA"] = '/home/user/PyRosetta.monolith.ubuntu.release-62'
#os.environ["PYROSETTA_DATABASE"] = '/home/user/PyRosetta.monolith.ubuntu.release-62/database'
from rosetta import *


def generate_resfile(resfile,mut_pos,mut_aa,chain_lig,job_folder):

    '''This method uses a provided resfile as a template to
    create the the mutation resfile
    returns the path of the file'''

    mut_resfile = open(job_folder+'/'+str(mut_pos)+'_'+mut_aa+'.mut_resfile','w')


    with open(resfile,'r') as input_file:
        for line in input_file:
            tab_data =line.split()
            if len(tab_data)>2:
                if tab_data[0].strip() == mut_post and  tab_data[1].strip() == chain_lig:
                    #This only works for Canonical AA
                    #print >>mut_resfile, ' %s %s  PIKAA %s EX 1 EX 2 EX 3' % (mut_pos,chain_lig,mut_aa)
                    print(' %s %s  EMPTY NC %s EX 1 EX 2 EX 3' % (mut_pos,chain_lig,mut_aa),file=mut_resfile)
                else:
                    line = line.strip()
                    print(line.replace('NATRO','NATAA EX 1 EX 2 EX 3'),file=mut_resfile)
            else:
                line = line.strip()
                print(line.replace('NATRO','NATAA EX 1 EX 2 EX 3'), file=mut_resfile)

    mut_resfile.close()

    return job_folder+'/'+str(mut_pos)+'_'+mut_aa+'.mut_resfile'


def mutate_residue(pose,scorefxn,resfile,cycles=3 ):

    #generate_resfile(  mutant_position, mutant_aa ,structure_params)
#  './'+name+'/'+name+'
    work_pose = Pose()
    work_pose.assign(pose)
    #Load resfile, and mod


    # mutate the position
    # CREATE TASK FOR DESIGN FROM EDITED RESFILE
    task_design = TaskFactory.create_packer_task(work_pose)
    parse_resfile(work_pose, task_design, resfile)
    import rosetta.core.pack.task
    rosetta.core.pack.task.parse_resfile(work_pose, task_design, resfile)
    #print task_design

    # USE DESIGN TASK TO CREATE PackResiduesMover
    packmover2 = PackRotamersMover(scorefxn, task_design)

    # CALCULATE ALL ROTAMERS
    packmover2.apply(work_pose)

    # work_pose = relaxation(work_pose,scorefxn,cycles=3)

    # print or save?
    logger.info('Mutation Score: '+str(scorefxn(work_pose)))

    return work_pose

def relaxation(a,scorefxn,cycles):

    # logger = logging.getLogger('binding_calc_and_mutation')

    pose = Pose()
    pose.assign(a)
    relax = ClassicRelax(scorefxn)
    relax.set_lj_ramp_cycles(3)
    relax.set_lj_ramp_inner_cycles(3)
    relax.set_stage2_cycles(10)
    relax.set_stage3_cycles(10)

    for i in range(cycles):
        relax.apply(pose)
        logger.info('Relaxation step: '+str(i)+' '+str(scorefxn(pose)))

    return pose

def repacking(pose,scorefxn):

    work_pose = Pose()
    work_pose.assign(pose)

    pose_packer = standard_packer_task(work_pose)

    pose_packer.restrict_to_repacking()    # turns off design
    pose_packer.or_include_current(True)    # considers original conformation

    packmover = PackRotamersMover(scorefxn, pose_packer)
    packmover.apply(work_pose)

    return work_pose


def calc_binding_energy(pose, scorefxn, replica, job_folder,cycles=10):
    # create a copy of the pose for manipulation
    work_pose = Pose()
    work_pose.assign(pose)

    # logger = logging.getLogger('binding_calc_and_mutation')


    logger.info('Initial_C '+str(scorefxn(work_pose)))


    work_pose = relaxation(work_pose,scorefxn,cycles)
    logger.info('PostRelax_C '+str(scorefxn(work_pose)))

    work_pose = repacking(work_pose,scorefxn)
    logger.info('PostRepack_C '+str(scorefxn(work_pose)))

    # dupl complex...
    work_pose.dump_pdb(job_folder+'/complex_relax_replica'+str(replica)+'.pdb')
    energy_complex = scorefxn(work_pose)
    # print >>output_log,'complex_energy: ',energy_complex


    # separate the docking partners
    #### since RigidBodyTransMover DOES NOT WORK, it is not used
    #transmover.apply(work_pose)

    # here are two methods for applying a translation onto a pose structure
    # both require an xyzVector
    #reinit the strucutre
    work_pose = Pose()
    work_pose.assign(pose)

    xyz = rosetta.numeric.xyzVector_double()    # a Vector for coordinates
    xyz.x = 500.0    # arbitrary separation magnitude, in the x direction
    xyz.y = 0.0    #...I didn't have this and it defaulted to 1e251...?
    xyz.z = 0.0    #...btw thats like 1e225 light years,
                   #    over 5e245 yrs at Warp Factor 9.999 (thanks M. Pacella)

    #### here is a hacky method for translating the downstream partner of a
    #    pose protein-protein complex (must by two-body!)
    chain2starts = len(pose.chain_sequence(1)) + 1
    for r in range(chain2starts, work_pose.total_residue() + 1):
        for a in range(1, work_pose.residue(r).natoms() + 1):
            work_pose.residue(r).set_xyz(a,
                work_pose.residue(r).xyz(a) + xyz)

    logger.info('Initial_F '+str(scorefxn(work_pose)))

    work_pose = relaxation(work_pose,scorefxn,cycles)
    logger.info('PostRelax_F '+str(scorefxn(work_pose)))

    work_pose = repacking(work_pose,scorefxn)
    logger.info('PostRepacking_F '+str(scorefxn(work_pose)))
    work_pose.dump_pdb(job_folder+'/free_relax_replica'+str(replica)+'pdb')
    energy_free_form = scorefxn(work_pose)

    logger.info(str(energy_complex)+'\t'+str(energy_free_form))
    # return the change in score COMPLEX - FREE
    return energy_complex,energy_free_form,energy_complex - energy_free_form

#@profile
def main(pdb,mut_post,mut_aa,replica,resfile,chain_lig,cycles):

    #need it for load all ncaa
    #Try to do not use it, , increase the memory needs of pyrosetta
    rosetta.init(extra_options='-override_rsd_type_limit')
    #rosetta.init()

    #load pdb
    pose = pose_from_pdb(pdb)

    #Output
    global_folder = pdb.split('.')[0]
    job_folder = './'+global_folder+'/'+str(mut_post)+'_'+str(mut_ncaa)
    output_ener = open('./'+global_folder+'/'+pdb+'_mut_'+str(mut_post)+'_'+str(mut_aa)+'_'+str(replica)+'_resfile.dat','w')

    #init scorefxn
    scorefxn = get_fa_scorefxn() #  create_score_function('standard')

    #create Mutation
    if mut_post != '0' and mut_aa != 'NO_MUT' and resfile != 'empty':
        #load resfile, and generate
        mut_resfile = generate_resfile(resfile,mut_post,mut_aa,chain_lig,job_folder)
        pose = mutate_residue(pose,scorefxn,mut_resfile)

    #Calc Delta Energy Binding (energy_complex - energy_free_form)
    energy_complex,energy_free_form,bE = calc_binding_energy(pose, scorefxn,replica,job_folder,cycles=cycles)
    # print out
    print(global_folder+'\t'+str(mut_post)+'\t'+str(mut_aa)+'\t'+str(energy_complex)+'\t'+str(energy_free_form)+'\t'+str(bE), file= output_ener)

if __name__ == '__main__':


#          PDB,       MUTATION     REPLICA
    #sys.argv[1],sys.argv[2],sys.argv[3])

    parser = optparse.OptionParser()
    parser.add_option('--pdb', dest = 'pdb_filename',
        default = '../test/data/test_in.pdb',    # default example PDB
        help = 'PDB file.This file is mandatory. ' )

    parser.add_option('--mut_pos', dest = 'mut_post',
        default = '0',    # default to False
        help = 'Mutation position, use pdb order and numbers. If is not provided the mutation is not performed')


    parser.add_option('--mut_aa', dest = 'mut_aa',
        default = 'NO_MUT',    # default to False
        help = 'Target amino acid in the 3 letter code. If is not provided the mutation is not performed')


    parser.add_option('--replica', dest = 'rep',
        default = '0',    # default to False
        help = 'replica number. If is not provided it will be labeled as 0')

    parser.add_option('--chain_lig', dest = 'chain_lig',
    default = '0',    # default to False
    help = 'Chain of the ligand or mutation molecule. This is mandatory.')


    parser.add_option('--resfile', dest = 'resfile',
        default = 'empty',    # default empty
        help = 'resfile, than belongs to the pdb. This file is mandatory. Use justresfile to create one')

    parser.add_option('--cycles', dest = 'cycles',
        default = 10, type='int',   # default 10
        help = 'Number of relaxation cycles, by default 10.')

    (options,args) = parser.parse_args()
    # PDB file option
    # Mandaroty
    pdb_filename = options.pdb_filename
    resfile = options.resfile

    if not os.path.isfile(pdb_filename):
        print('Error reading the PDB file')
        parser.print_help()
        sys.exit()
    if not os.path.isfile(resfile):
        print('Error reading the resfile')
        parser.print_help()
        sys.exit()

    #Mutation info
    mut_post = options.mut_post
    mut_ncaa = options.mut_aa
    chain_lig = options.chain_lig
    #Others
    replica_num = options.rep
    cycles = options.cycles
    #Performance
    start_time = time.time()
    #Work folder Defintion
    global_folder = pdb_filename.split('.')[0]
    job_folder = './'+global_folder+'/'+str(mut_post)+'_'+str(mut_ncaa)

    try:
        os.stat(global_folder)
    except:
        os.mkdir(global_folder)

    try:
        os.stat(job_folder)
    except:
        os.mkdir(job_folder)

    try:
        os.stat('logs')
    except:
        os.mkdir('logs')

    
    #Set up logging
    # create logger with 'spam_application'
    logger = logging.getLogger('binding_calc_and_mutation')
    logger.setLevel(logging.DEBUG)
    # # create file handler which logs even debug messages
    # Collect them in the same directory is much better solution to control them
    fh = logging.FileHandler('./logs/'+pdb_filename+'_mut_'+mut_post+'_'+mut_ncaa+'_'+replica_num+'_resfile.log')
    fh.setLevel(logging.DEBUG)
    # # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    # # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)
    logger.info(pdb_filename+' '+str(mut_post)+' '+mut_ncaa+' '+str(replica_num))

    main(pdb_filename,mut_post,mut_ncaa,replica_num,resfile,chain_lig,cycles)

    #Performce references
    logger.info('Time : '+str((time.time() - start_time)))