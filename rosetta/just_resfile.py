
import sys
import os
#DYLD_LIBRARY_PATH

#os.environ["PYROSETTA"] = '/home/user/PyRosetta.monolith.ubuntu.release-62'
#os.environ["PYROSETTA_DATABASE"] = '/home/user/PyRosetta.monolith.ubuntu.release-62/database'

from rosetta import *
rosetta.init(extra_options='-override_rsd_type_limit')

# CLEAN UP PDB FOR ROSETTA
from toolbox import cleanATOM

cleanATOM(sys.argv[1])

nam = sys.argv[1].split('.')[0]
# PUT PDB + INFERRED INFO INTO DATA CONTAINER
pose = pose_from_pdb(sys.argv[1])
#print pose

# CALCULATE ROSETTA SCORE FOR STRUCTURE (fa = full atom)
scorefxn = get_fa_scorefxn()
#print scorefxn
#print scorefxn(pose) # CALCULATE
#scorefxn.show(pose) # SHOW BREAKDOWN

# CREATE RESFILE FROM POSE
from toolbox import generate_resfile_from_pose
generate_resfile_from_pose(pose, nam+".resfile")

# RELAX THE PDB
#relax = ClassicRelax()
#relax.set_scorefxn(scorefxn)
#relax.apply(pose)

# CALCULATE ROSETTA SCORE ON RELAXED PDB (fa = full atom)
#print nam
#print 'Post relaxation score:', scorefxn(pose), nam

# WRITE NEW DESIGNED & RELAXED PDB
#pose.dump_pdb(nam+"_relaxed.pdb")
