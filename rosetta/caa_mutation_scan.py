from __future__ import print_function
from rosetta import *
import sys
import os

rosetta.init()

def work_folder(pep):
    try:
        os.stat(pep)
    except:
        os.mkdir(pep)
    return

def generate_resfile(workdir,pep,template='VQSVEKILLSVVSMLA'):
    resfile = open(workdir+'/'+pep+'.resfile','w')
    print("USE_INPUT_SC", file=resfile)
    print("start", file=resfile)

    chain_mod = 85
    chain_lig = 'A'
    chain_prot = 'B'

    for i in range(len(pep)):
        if pep[i] == template[i]:

            print(' %i %s  NATAA EX 1 EX 2' % (i+chain_mod,chain_lig), file=resfile)
        else:
            print(' %i %s  PIKAA %s EX 1 EX 2' % (i+chain_mod,chain_lig,pep[i]), file=resfile)

    for i in range(138,229+1):
        print(' %i %s  NATAA EX 1 EX 2' % (i,chain_prot), file=resfile)

    resfile.close()
    return


def do_calcs(pose,outputfile,pep,workfolder):

    

    # CALCULATE ROSETTA SCORE FOR STRUCTURE (fa = full atom)
    scorefxn = get_fa_scorefxn()
    
    print(pep,'pre-initial structure score:', scorefxn(pose), file=outputfile)
    scorefxn.show(pose) # SHOW BREAKDOWN

    # CREATE TASK FOR DESIGN FROM EDITED RESFILE
    task_design = TaskFactory.create_packer_task(pose)
    parse_resfile(pose, task_design, workfolder+pep+'.resfile')
    import rosetta.core.pack.task
    rosetta.core.pack.task.parse_resfile(pose, task_design, workfolder+pep+'.resfile')
    #print task_design

    # USE DESIGN TASK TO CREATE PackResiduesMover
    packmover2 = PackRotamersMover(scorefxn, task_design)

    # CALCULATE ALL ROTAMERS
    packmover2.apply(pose)
    print(outputfile,pep,'Post packing score:', scorefxn(pose),file=outputfile)
    # print pose # TO GET SEQ
    scorefxn.show(pose) #  SHOW BREAKDOWN

    # RELAX THE DESIGNED PDB
    relax = ClassicRelax()
    relax.set_scorefxn(scorefxn)
    relax.apply(pose)

    # WRITE NEW DESIGNED & RELAXED PDB
    pose.dump_pdb(workfolder+pep+"_designed.pdb")

    # CALCULATE ROSETTA SCORE ON DESIGNED & RELAXED PDB (fa = full atom)
    print(pep,'post design & relax score:', scorefxn(pose), file=outputfile)


    return



# PUT PDB + INFERRED INFO INTO DATA CONTAINER

pose = pose_from_pdb(sys.argv[1])
# extract name 
nam = sys.argv[1].split('.')[0]
# peptide seq
pep_template = 'VQSVEKILLSVVSMLA'
# list of residues to try
res_list= ['A',
                'R',
                'N',
                'D',
                'C',
                'E',
                'Q',
                'G',
                'H',
                'I',
                'L',
                'K',
                'M',
                'F',
                'P',
                'S',
                'T',
                'W',
                'Y',
                'V']

try:
    os.stat(nam)
except:
    os.mkdir(nam)

for j in range(len(pep_template)):
    for a in res_list:
        mut_pep = pep_template
        if not mut_pep[j] == a:
            mut_pep[j] = a
            outputfile = open(mut_pep+'.out','w')

            workdir= './{}/{}/'.format(nam, mut_pep)
            work_folder(workdir)
            generate_resfile(workdir,mut_pep,pep_template)
            do_calcs(pose,outputfile,mut_pep,workdir)

            outputfile.close()














